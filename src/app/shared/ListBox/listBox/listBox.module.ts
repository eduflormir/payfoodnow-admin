import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material';
import { ListBoxComponent } from './listBox.component';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    DragDropModule
  ],
  declarations: [
    ListBoxComponent
  ],
  exports: [
    ListBoxComponent
  ]

})
export class ListBoxModule { }
