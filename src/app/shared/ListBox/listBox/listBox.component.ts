import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";
import { AsignarMesaCamareroService } from "src/app/services/asignarMesaCamarero.service";
import { Mesa } from "src/app/models/Mesa";
import { Camarero } from "src/app/models/Camarero";
import { CamareroMesa } from "src/app/models/CamareroMesa";
import { MesaAsignada } from 'src/app/models/MesaAsignada';

@Component({
  selector: "app-listBox",
  templateUrl: "./listBox.component.html",
  styleUrls: ["./listBox.component.css"]
})
export class ListBoxComponent implements OnInit {
  mesasDisponibles: Array<Mesa>;
  mesasAsignadas: Array<MesaAsignada>;
  mesaSeleccionada: any = {
    IdMesaCamarero: null
  }
  camareroMesa: CamareroMesa;
  idCamarero: number;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public camarero: Camarero,
    private asignarMesaCamareroService: AsignarMesaCamareroService
  ) {}

  ngOnInit() {
    this.idCamarero = this.camarero.IdUsuario;

    if (this.camarero) {
      this.getMesasDisponibles();
      this.getMesasAsignadas();
    }
  }

  private getMesasDisponibles(): any {
    this.asignarMesaCamareroService
      .getMesasDisponibles(this.camarero)
      .subscribe((data: any) => {
        this.mesasDisponibles = data;
        console.log("MesasDisponibles:", data);
      });
  }

  private getMesasAsignadas(): any {
    this.asignarMesaCamareroService
      .getMesasAsignadas(this.camarero)
      .subscribe((data: any) => {
        this.mesasAsignadas = data;
        console.log("MesasAsignadas:", data);
      });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    const previousData = event.previousContainer.data;
    const currentData = event.container.data;

    const previousIndex = event.previousIndex;
    const currentIndex = event.currentIndex;

    let dataToApi = currentData.slice(currentIndex, currentIndex + 1);

    this.mesaSeleccionada = dataToApi.pop();
    const idMesa = this.mesaSeleccionada.Id;

    // if ( this.mesaSeleccionada.idMesaCamarero == 0) {
    //   this.mesaSeleccionada.idMesaCamarero = null;
    // } else {
    //   this.mesaSeleccionada = this.mesaSeleccionada.idMesaCamarero;
    // }

    console.log('IDMESACAMARERO', this.mesaSeleccionada);



    const id = Number(event.container.id);

    console.log("Id", id, typeof id);

    // Si el id es igual a 0 entonces habrá que añadir a las mesas mesasDisponibles
    // la mesa seleccionada y por otro lado tendremos que quitar a las mesas
    // mesasAsignadas la misma mesas

    if (id === 0) {
      console.log('Vamos a dejar libre una mesa');
      const IdCamareroMesa = this.mesaSeleccionada.IdCamareroMesa;
      console.log('....', IdCamareroMesa)

      this.asignarMesaCamareroService
        .UnAssignedTable(this.mesaSeleccionada.IdCamareroMesa)
        .subscribe(data => {
          console.log("Desasignando mesa a camarero", data);
        });

      // const camareroMesa = new CamareroMesa(
      //   this.mesaSeleccionada.Id,
      //   this.camarero.Id,
      //   this.camarero.Id
      // );
      // this.asignarMesaCamareroService
      //   .SetUnAssignedTable(camareroMesa)
      //   .subscribe(data => {
      //     console.log("Asignando mesa a camarero", data);
      //   });
    } else {
      console.log('Vamos a asignar una mesa');
      const camareroMesa = new CamareroMesa(
        this.mesaSeleccionada.IdCamareroMesa,
         this.camarero.IdUsuario,
         this.mesaSeleccionada.IdMesa
       );

      this.asignarMesaCamareroService
      .SetAssignedTable(camareroMesa)
      .subscribe(data => {
      console.log("Asignando mesa a camarero", data);
    });

    }

    console.log("DataToApi", dataToApi);
    console.log("StringToApi", this.mesaSeleccionada.Id);
  }
}
