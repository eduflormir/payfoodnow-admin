import { Component, OnInit, OnDestroy } from "@angular/core";
import { CamarerosService } from "../camareros.service";
import { Router } from "@angular/router";
import { Constants } from "src/app/core/Constants";
import { Subscription } from "rxjs";
import { Camarero } from 'src/app/models/Camarero';

@Component({
  selector: "app-lista-camareros",
  templateUrl: "./lista-camareros.component.html",
  styleUrls: ["./lista-camareros.component.scss"]
})
export class ListaCamarerosComponent implements OnInit, OnDestroy {
  private idLocal: number;
  private listadoCamareros = [];
  subscriptorEventoUpdateCamarero: Subscription;

  arrayCamareros = [
    {
      id: 1,
      nombre: "Eduardo",
      password: "12134"
    },
    {
      id: 2,
      nombre: "Carmen",
      password: "hola"
    }
  ];

  constructor(
    private camarerosService: CamarerosService,
    private router: Router
  ) {}

  ngOnInit() {
    // Cuando se renderiza la vista
    const idLocal = Number(localStorage.getItem(Constants.IdLocal));
    this.getAll(idLocal);

    // Está escuchando el evento de eliminar un camarero y actualiza el listado de camareros
    this.subscriptorEventoUpdateCamarero = this.camarerosService
      .eventoCamareroSubject$()
      .subscribe(data => {
        this.getAll(idLocal);
      });
  }

  private getAll(idLocal: number) {
    this.camarerosService.getAllCamareros(idLocal).subscribe(
      (res: Array<Camarero>) => {
        this.listadoCamareros = res;
      },
      (error: any) => {
        console.log('ERROR', error);
      }
    );
  }

  ngOnDestroy() {
    this.subscriptorEventoUpdateCamarero.unsubscribe();
  }
}
