import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCamarerosComponent } from './lista-camareros.component';

describe('ListaCamarerosComponent', () => {
  let component: ListaCamarerosComponent;
  let fixture: ComponentFixture<ListaCamarerosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCamarerosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCamarerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
