import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarMesaCamareroComponent } from './asignar-mesa-camarero.component';

describe('AsignarMesaCamareroComponent', () => {
  let component: AsignarMesaCamareroComponent;
  let fixture: ComponentFixture<AsignarMesaCamareroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarMesaCamareroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarMesaCamareroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
