import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CamarerosRoutingModule } from './camareros-routing.module';
import { ListaCamarerosComponent } from './lista-camareros/lista-camareros.component';
import { DetalleCamarerosComponent } from './detalle-camareros/detalle-camareros.component';
import { NuevoCamareroComponent } from './nuevo-camarero/nuevo-camarero.component';

import { AsignarMesaCamareroComponent } from './asignar-mesa-camarero/asignar-mesa-camarero.component';
import { MatInputModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CamareroComponent } from './camarero/camarero.component';
import { ListBoxModule } from '../shared/ListBox/listBox/listBox.module';


@NgModule({
  declarations: [
    ListaCamarerosComponent,
    DetalleCamarerosComponent,
    NuevoCamareroComponent,
    DetalleCamarerosComponent,
    AsignarMesaCamareroComponent,
    CamareroComponent],
  imports: [
    CommonModule,
    CamarerosRoutingModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule

  ]
})
export class CamarerosModule { }
