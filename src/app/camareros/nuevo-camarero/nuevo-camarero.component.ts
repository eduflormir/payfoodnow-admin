import { Component, OnInit } from '@angular/core';
import { Camarero } from 'src/app/models/Camarero';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CamarerosService } from '../camareros.service';
import { Constants } from 'src/app/core/Constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-camarero',
  templateUrl: './nuevo-camarero.component.html',
  styleUrls: ['./nuevo-camarero.component.scss']
})
export class NuevoCamareroComponent implements OnInit {
  formularioCreateCamarero: FormGroup;
  camarero: Camarero;

  constructor(
    private fb: FormBuilder,
    private camarerosService: CamarerosService,
    private router: Router
  ) { }

  ngOnInit() {
    this.camarero = new Camarero();

    this.formularioCreateCamarero = this.fb.group({
      Nombre: '',
      Apellidos: '',
      Login: '',
      Password: ''
    });
  }

  CreateCamarero() {
    this.camarero = this.formularioCreateCamarero.value;
    this.camarero.IdLocal = Number(localStorage.getItem(Constants.IdLocal));

    this.camarerosService.createCamarero(this.camarero)
      .subscribe((res: any) => {
        console.log('Respuesta create Camarero', res);
        this.router.navigate(['privado', 'camareros']);
      });
  }

}
