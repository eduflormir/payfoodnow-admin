import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoCamareroComponent } from './nuevo-camarero.component';

describe('NuevoCamareroComponent', () => {
  let component: NuevoCamareroComponent;
  let fixture: ComponentFixture<NuevoCamareroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoCamareroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoCamareroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
