import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleCamarerosComponent } from './detalle-camareros.component';

describe('DetalleCamarerosComponent', () => {
  let component: DetalleCamarerosComponent;
  let fixture: ComponentFixture<DetalleCamarerosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleCamarerosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleCamarerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
