import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CamarerosService } from '../camareros.service';
import { Camarero } from 'src/app/models/Camarero';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-detalle-camareros',
  templateUrl: './detalle-camareros.component.html',
  styleUrls: ['./detalle-camareros.component.scss']
})
export class DetalleCamarerosComponent implements OnInit {
  @Input() camarero: Camarero;
  formularioUpdateCamarero: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private camarerosService: CamarerosService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    console.log('El detalle de camarero', this.camarero);
    this.route.queryParamMap.subscribe((data: any) => {
      this.camarerosService.getDetailCamarero(data.params.id)
        .subscribe( (res) => {
          this.camarero = res;
          console.log('Detalle de la camarero', this.camarero);

          this.formularioUpdateCamarero = this.fb.group({
            Id: this.camarero.Id,
            IdLocal: this.camarero.IdLocal,
            Nombre: [this.camarero.Nombre, Validators.required],
            Apellidos: this.camarero.Apellidos,
            Password: this.camarero.Password,
            IdUsuario: this.camarero.IdUsuario,
            Login: this.camarero.Login,
            IdTpv: this.camarero.IdTpv
          });
    },
    error => console.log('ERROR', error));
});
  }

  updateDetailCamarero() {
    if ( this.formularioUpdateCamarero.valid) {

      const camarero = this.formularioUpdateCamarero.value;
      this.camarerosService.updateDetailCamarero(camarero)
        .subscribe( (respuesta: any) => {
          console.log('Respuesta de la llamada update detalle', respuesta);
          this.router.navigate(['privado', 'camareros']);
        });


    } else {
      console.log('¿ QUE HA PSADO?')
    }

  }
}

