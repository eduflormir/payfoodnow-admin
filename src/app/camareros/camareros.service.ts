import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Camarero } from '../models/Camarero';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Constants } from '../core/Constants';

@Injectable({
  providedIn: 'root'
})
export class CamarerosService {
public url = environment.url;
public params = new HttpParams();

eventoCamareroSubject = new Subject<boolean>();

  constructor(
    private http: HttpClient
  ) { }

  public eventoCamareroSubject$() {
   return this.eventoCamareroSubject.asObservable();
  }

  eventoClickCamarero(valor: boolean) {
   return this.eventoCamareroSubject.next(valor);
  }

  public getAllCamareros(idLocal: number): Observable<any> {
    console.log('idLocal', idLocal);
    return this.http.post(`${this.url}/${Constants.urlApiCamareros}/GetAll`,  idLocal );

  }

  public getDetailCamarero(idCamarero: string): Observable<any> {
    if ( this.params.has('idCamarero')) {
      this.params = this.params.delete('idCamarero');
      this.params = this.params.append('idCamarero', idCamarero);
      return this.http.get(`${this.url}/${Constants.urlApiCamareros}/GetDetail`, { params: this.params} );
    } else {
      this.params = this.params.append('idCamarero', idCamarero);
      return this.http.get(`${this.url}/${Constants.urlApiCamareros}/GetDetail`, { params: this.params} ); // QueryPamars
    }
  }

  public updateDetailCamarero(camarero: Camarero): Observable<any> {
    return this.http.put(`${this.url}/${Constants.urlApiCamareros}/CreateUpdate`, camarero);
  }

  public createCamarero(camarero: Camarero): Observable<any> {
    return this.http.put(`${this.url}/${Constants.urlApiCamareros}/CreateUpdate`, camarero);
  }

  public deleteCamarero(idCamarero): Observable<any> {
    if ( this.params.has('idCamarero')) {
      this.params = this.params.delete('idCamarero');
      this.params = this.params.append('idCamarero', idCamarero);
      return this.http.delete(`${this.url}/${Constants.urlApiCamareros}/Delete`, { params: this.params});
    } else {
      this.params = this.params.append('idCamarero', idCamarero);
      return this.http.delete(`${this.url}/${Constants.urlApiCamareros}/Delete`, { params: this.params});
    }
  }


}
