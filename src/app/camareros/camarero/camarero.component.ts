import { Component, OnInit, Input } from '@angular/core';
import { Camarero } from 'src/app/models/Camarero';
import { Router } from '@angular/router';
import { CamarerosService } from '../camareros.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ListBoxComponent } from 'src/app/shared/ListBox/listBox/listBox.component';


@Component({
  selector: 'app-camarero',
  templateUrl: './camarero.component.html',
  styleUrls: ['./camarero.component.css']
})
export class CamareroComponent implements OnInit {
  @Input() camarero: Camarero;

  constructor(
    private router: Router,
    private camarerosService: CamarerosService,
    private modal: MatDialog
  ) { }

  ngOnInit() {

  }

  verDetalleCamarero(idCamarero: number) {
    this.router.navigate(['privado', 'camareros', 'detalleCamarero'], { queryParams: { id: idCamarero}});
  }

  deleteCamarero(idCamarero: number) {
    this.camarerosService.deleteCamarero(idCamarero).subscribe((respuesta) => {
      this.camarerosService.eventoClickCamarero(true);
    });
  }

  abrirModal(idCamarero: number) {
    const modal = this.modal.open( ListBoxComponent, {
       width: '1200px',
      data: this.camarero
    });
  }

}
