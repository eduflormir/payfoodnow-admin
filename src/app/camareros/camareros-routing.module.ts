import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaCamarerosComponent } from './lista-camareros/lista-camareros.component';
import { NuevoCamareroComponent } from './nuevo-camarero/nuevo-camarero.component';
import { DetalleCamarerosComponent } from './detalle-camareros/detalle-camareros.component';
import { AsignarMesaCamareroComponent } from './asignar-mesa-camarero/asignar-mesa-camarero.component';

const routes: Routes = [
  {
    path: 'detalleCamarero', component: DetalleCamarerosComponent
  },
  {
    path: 'nuevoCamarero', component: NuevoCamareroComponent
  },
  {
    path: 'asignarMesa', component: AsignarMesaCamareroComponent
  },
  {
    path: '', component: ListaCamarerosComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CamarerosRoutingModule { }
