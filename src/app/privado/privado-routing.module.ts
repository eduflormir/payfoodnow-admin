import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivadoComponent } from './privado/privado.component';
import { PrincipalComponent } from '../principal/principal.component';


const routesAdmin: Routes = [
{
  path: '', component: PrivadoComponent,
  children: [
    {
      path: 'principal', component: PrincipalComponent
    },
    {
      path: 'mesas' , loadChildren: '../mesas/mesas.module#MesasModule'
    },
    {
      path: 'camareros' , loadChildren: '../camareros/camareros.module#CamarerosModule'
    },
    {
      path: 'gerente' , loadChildren: '../gerente/gerente.module#GerenteModule'
    },
    {
      path: 'productos' , loadChildren: '../productos/productos.module#ProductosModule'
    }
  ]

}
];

@NgModule({
  imports: [RouterModule.forChild(routesAdmin)],
  exports: [RouterModule]
})
export class PrivadoRoutingModule { }
