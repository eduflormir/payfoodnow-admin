import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivadoRoutingModule } from './privado-routing.module';
import { PrivadoComponent } from './privado/privado.component';
import { MesasModule } from '../mesas/mesas.module';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PrincipalComponent } from '../principal/principal.component';


@NgModule({
  declarations: [
    PrivadoComponent,
    NavbarComponent,
    SidebarComponent,
    PrincipalComponent
 

  ],
  imports: [
    CommonModule,
    PrivadoRoutingModule,
    MesasModule

  ]
})
export class PrivadoModule { }
