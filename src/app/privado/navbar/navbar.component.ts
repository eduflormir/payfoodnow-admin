import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/general/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private _usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  salir() {
    this._usuarioService.isLogin(false);
    this.router.navigate(['login']);
  }

}
