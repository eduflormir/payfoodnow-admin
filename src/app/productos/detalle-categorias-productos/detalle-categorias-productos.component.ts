import { Component } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-detalle-categorias-productos',
  templateUrl: './detalle-categorias-productos.component.html',
  styleUrls: ['./detalle-categorias-productos.component.scss']
})

export class DetalleCategoriasProductosComponent {
 
  mesasAsigandas = [

  ];

  mesasDisponibles = [
    'Mesa 1',
    'Mesa 2',
    'Mesa 3',
    'Mesa 4',
    'Mesa 5',
  ];

  drop(event: CdkDragDrop<string[]>) {
    console.log('Que soy:', event)
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    console.log('Actualizamos la API => Es el array de mesas asigandas', event.container.data)
    console.log('Actualizamos la API => Es el array de mesas disponibles', event.previousContainer.data)


  }

}


