import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleCategoriasProductosComponent } from './detalle-categorias-productos.component';

describe('DetalleCategoriasProductosComponent', () => {
  let component: DetalleCategoriasProductosComponent;
  let fixture: ComponentFixture<DetalleCategoriasProductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleCategoriasProductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleCategoriasProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
