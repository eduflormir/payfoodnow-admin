import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCategoriasProductosComponent } from './lista-categorias-productos.component';

describe('ListaCategoriasProductosComponent', () => {
  let component: ListaCategoriasProductosComponent;
  let fixture: ComponentFixture<ListaCategoriasProductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCategoriasProductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCategoriasProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
