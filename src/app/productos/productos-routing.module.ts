import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalleCategoriasProductosComponent } from './detalle-categorias-productos/detalle-categorias-productos.component';
import { ListaCategoriasProductosComponent } from './lista-categorias-productos/lista-categorias-productos.component';


const routes: Routes = [
  {
    path: '', component: ListaCategoriasProductosComponent
  },
  {
    path: 'detalleCategorias', component: DetalleCategoriasProductosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
