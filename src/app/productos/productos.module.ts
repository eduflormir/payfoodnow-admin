import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { DetalleCategoriasProductosComponent } from './detalle-categorias-productos/detalle-categorias-productos.component';
import { ListaCategoriasProductosComponent } from './lista-categorias-productos/lista-categorias-productos.component';

import { DragDropModule } from '@angular/cdk/drag-drop';




@NgModule({
  declarations: [ DetalleCategoriasProductosComponent, ListaCategoriasProductosComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    DragDropModule
  ]
})
export class ProductosModule { }
