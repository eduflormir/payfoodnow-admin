import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Mesa } from '../models/Mesa';
import { Camarero } from '../models/Camarero';
import { CamareroMesa } from '../models/CamareroMesa';

@Injectable({
  providedIn: 'root'
})
export class AsignarMesaCamareroService {
URL = environment.url;

constructor(
  private http: HttpClient
) { }

public getMesasDisponibles(camarero: Camarero): any {
  const params = new HttpParams()
  .set('idLocal', String(camarero.IdLocal));

  return this.http.get(`${this.URL}/api/Mesas/GetAvailableTables`, {params});

}

public getMesasAsignadas(camarero: Camarero): any {
  const params = new HttpParams()
  .set('idCamarero', String(camarero.Id));

  return this.http.get(`${this.URL}/api/Mesas/GetAssignedTables`, { params });

}

public SetAssignedTable( camareroMesa: CamareroMesa) {
  return this.http.put(`${this.URL}/api/Mesas/SetAssignedTable`, camareroMesa);
}

public UnAssignedTable( idCamareroMesa: number) {
  console.log("UnAssignedTable, idCamareroMesa:",idCamareroMesa);

  return this.http.put(`${this.URL}/api/Mesas/UnAssignedTable`,  idCamareroMesa );
}

}
