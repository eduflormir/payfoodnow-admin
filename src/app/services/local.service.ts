import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalService {
public url = environment.url;
public params = new HttpParams();
public IdLocal: number;

constructor( private http: HttpClient) { }

public getLocal(valor) {
  this.params = this.params.append('idLocal', valor);
  return this.http.get(`${this.url}/api/Local/`, { params: this.params} );
}

public getAll(idUsuario) {
  console.log('Hola', idUsuario)
  this.params = this.params.append('idUsuario', idUsuario);
  return this.http.get(`${this.url}/api/Local/GetAll`, { params: this.params} );
}







}

