import { Injectable, OnDestroy } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError, from} from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class TokenInterceptor implements HttpInterceptor, OnDestroy {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({headers: request.headers.set('Accept', 'application/json')});
    // console.log('REQUEST', request);

    return next.handle(request).pipe(
      catchError( (err) => {
        console.log('Error desde Interceptor', err.error.message);
        return throwError(err);
      })
    );
}





  ngOnDestroy() { }
}
