import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MesasRoutingModule } from './mesas-routing.module';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import { ListaMesasComponent } from './lista-mesas/lista-mesas.component';
import { DetalleMesasComponent } from './detalle-mesas/detalle-mesas.component';
import { NuevaMesaComponent } from './nueva-mesa/nueva-mesa.component';
import { MesaComponent } from './mesa/mesa.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListaMesasComponent, DetalleMesasComponent, NuevaMesaComponent, MesaComponent],
  imports: [
    CommonModule,
    MesasRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    ListaMesasComponent
  ]
})
export class MesasModule { }
