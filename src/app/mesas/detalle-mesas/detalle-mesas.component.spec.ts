import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleMesasComponent } from './detalle-mesas.component';

describe('DetalleMesasComponent', () => {
  let component: DetalleMesasComponent;
  let fixture: ComponentFixture<DetalleMesasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleMesasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleMesasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
