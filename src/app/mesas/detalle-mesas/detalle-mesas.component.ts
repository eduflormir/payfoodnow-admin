import { Component, OnInit, Input } from '@angular/core';
import { MesasService } from '../mesas.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Mesa } from '../../models/Mesa';

@Component({
  selector: 'app-detalle-mesas',
  templateUrl: './detalle-mesas.component.html',
  styleUrls: ['./detalle-mesas.component.scss']
})
export class DetalleMesasComponent implements OnInit {
  mesa: Mesa;
  idMesa: any;
  formularioUpdateMesa: FormGroup;

  constructor(
    private mesasService: MesasService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    console.log(this.mesa)

    // ESTO EN EL CASO DE PASAR COMO UN PARÁMETRO DENTRO DE LA URL
    // this.route.params.subscribe((data) => {
    //   console.log('ID', data)
    //   this.mesasService.getDetailMesa(data.id)
    //     .subscribe( (res) => {
    //       this.detalleMesa = res;
    //       console.log('Detalle de la mesa', this.detalleMesa);
    //     },
    //     error => console.log('ERROR', error));
    // });

    this.route.queryParamMap.subscribe((data: any) => {
          this.mesasService.getDetailMesa(data.params.id)
            .subscribe( (res) => {
              this.mesa = res;
              console.log('Detalle de la mesa', this.mesa);

              this.formularioUpdateMesa = this.formBuilder.group({
                Id: this.mesa.Id,
                IdLocal: this.mesa.IdLocal,
                NumMesa: this.mesa.NumMesa,
                NumSitios: this.mesa.NumSitios,
                Ubicacion: this.mesa.Ubicacion,
                IdMesaEstado: this.mesa.IdMesaEstado,
                DesMesaEstado: this.mesa.DesMesaEstado
              });
        },
        error => console.log('ERROR', error));
    });

  }

  updateDetailMesa() {
    this.mesasService.updateDetailMesa(this.formularioUpdateMesa.value)
      .subscribe((data) => {
        console.log('Respuesta detalle mesa', data)
      });
  }

}
