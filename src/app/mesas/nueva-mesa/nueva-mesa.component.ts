import { Component, OnInit } from '@angular/core';
import { MesasService } from '../mesas.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Mesa } from 'src/app/models/Mesa';
import { stringify } from 'querystring';
import { Constants } from 'src/app/core/Constants';

@Component({
  selector: 'app-nueva-mesa',
  templateUrl: './nueva-mesa.component.html',
  styleUrls: ['./nueva-mesa.component.scss']
})
export class NuevaMesaComponent implements OnInit {

  formularioCreateMesa: FormGroup;
  mesa: Mesa = new Mesa();
  private IdLocal = Number(localStorage.getItem(Constants.IdLocal));

  constructor(
    private formBuilder: FormBuilder,
    private mesasService: MesasService
  ) { }

  ngOnInit() {

    this.formularioCreateMesa = this.formBuilder.group({
      Id: this.mesa.Id,
      IdLocal: this.IdLocal,
      NumMesa: this.mesa.NumMesa,
      NumSitios: this.mesa.NumSitios,
      Ubicacion: this.mesa.Ubicacion,
      IdMesaEstado: Constants.IdEstadoMesaLibre
    });
  }

  public createMesa() {
    this.mesasService.createMesa(this.formularioCreateMesa.value)
    .subscribe(
      (data) => {console.log('Respuesta create mesa', data);
  });
  }

}
