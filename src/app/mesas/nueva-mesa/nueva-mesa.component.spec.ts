import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaMesaComponent } from './nueva-mesa.component';

describe('NuevaMesaComponent', () => {
  let component: NuevaMesaComponent;
  let fixture: ComponentFixture<NuevaMesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaMesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaMesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
