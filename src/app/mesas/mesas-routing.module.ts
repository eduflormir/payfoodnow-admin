import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaMesasComponent } from './lista-mesas/lista-mesas.component';
import { NuevaMesaComponent } from './nueva-mesa/nueva-mesa.component';
import { DetalleMesasComponent } from './detalle-mesas/detalle-mesas.component';

const routes: Routes = [
  {
    path: '', component: ListaMesasComponent
  },
  {
    // path: 'detalleMesas/:id', component: DetalleMesasComponent
       path: 'detalleMesas', component: DetalleMesasComponent

  },
  {
    path: 'nuevaMesa', component: NuevaMesaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MesasRoutingModule { }
