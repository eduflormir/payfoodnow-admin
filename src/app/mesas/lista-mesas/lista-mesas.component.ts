import { Component, OnInit, OnDestroy } from '@angular/core';
import { MesasService } from '../mesas.service';
import { Mesa } from 'src/app/models/Mesa';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-lista-mesas',
  templateUrl: './lista-mesas.component.html',
  styleUrls: ['./lista-mesas.component.scss']
})
export class ListaMesasComponent implements OnInit, OnDestroy {

  private idLocal: number;
  private listadoMesas = [];
  subscriptorEventoUpdateMesa: Subscription;

  constructor(
    private mesasService: MesasService,
    private router: Router

  ) { }

  ngOnInit() {
    this.idLocal = Number(localStorage.getItem('idLocal'));
    this.getAll(this.idLocal);
    this.subscriptorEventoUpdateMesa = this.mesasService.eventoDeleteMesaSubject$()
      .subscribe( (respuesta) => {
        this.getAll(this.idLocal);
      });

  }

  private getAll(idLocal) {
    this.mesasService.getAllMesas(idLocal)
      .subscribe( (res: any) => {
        console.log('REs', res)
        this.listadoMesas = res;
      },
      error => {
        console.log('ERROR', error);
      });
  }

  ngOnDestroy(): void {
    this.subscriptorEventoUpdateMesa.unsubscribe();
  }


}
