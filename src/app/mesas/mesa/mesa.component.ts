import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Mesa } from '../../models/Mesa';
import { MesasService } from '../mesas.service';
import { Constants } from 'src/app/core/Constants';

@Component({
  selector: 'app-mesa',
  templateUrl: './mesa.component.html',
  styleUrls: ['./mesa.component.css']
})
export class MesaComponent implements OnInit {
  @Input() mesa: Mesa;
  IdMesaEstado = Constants.IdEstadoMesaLibre;

  constructor(
    private mesaService: MesasService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  irDetalleMesa(idMesa) {
    this.router.navigate(['privado', 'mesas' , 'detalleMesas'], { queryParams: { id: idMesa}});

  }

  deleteMesa() {
    this.mesaService.deleteMesa(this.mesa.Id)
      .subscribe((respuesta) => {
        this.mesaService.eventoDeleteMesa(true);
      });
  }

  liberateMesa() {
    this.mesaService.liberateMesa(this.mesa.Id)
      .subscribe((respuesta) => {
        this.mesaService.eventoDeleteMesa(true);
      });
  }
}
