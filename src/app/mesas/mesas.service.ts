import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Mesa } from '../models/Mesa';

@Injectable({
  providedIn: 'root'
})
export class MesasService {

public url = environment.url;
public urlPayFoodNowWebApi = environment.UrlPayFoodNowWebApi;
public params = new HttpParams();

eventoDeleteMesaSubject = new Subject<any>();

constructor( private http: HttpClient) { }

public eventoDeleteMesaSubject$() {
  return this.eventoDeleteMesaSubject.asObservable();
}

public eventoDeleteMesa(valor: boolean) {
  return this.eventoDeleteMesaSubject.next(valor);
}

public getAllMesas(idLocal): Observable<any> {
  if(this.params.has('idLocal')){
    this.params = this.params.delete('idLocal');
    this.params = this.params.append('idLocal', idLocal);
    return this.http.get(`${this.url}/api/Mesas/GetAll`,  { params: this.params} );
  } else {
    this.params = this.params.append('idLocal', idLocal);
    return this.http.get(`${this.url}/api/Mesas/GetAll`,  { params: this.params} );
  }

}

public getDetailMesa(idMesa): Observable<any> {
  if ( this.params.has('idMesa')) {
    this.params = this.params.delete('idMesa');
    this.params = this.params.append('idMesa', idMesa);
    return this.http.get(`${this.url}/api/Mesas/GetDetail`, { params: this.params} );
  } else {
    this.params = this.params.append('idMesa', idMesa);
    return this.http.get(`${this.url}/api/Mesas/GetDetail`, { params: this.params} ); // QueryPamars
  }
  // return this.http.get(`${this.url}/api/Mesas/GetDetail/${idMesa}` ); // Este caso sería como un parámetro de la url
}

public updateDetailMesa(mesa): Observable<any> {
  return this.http.put(`${this.url}/api/Mesas/CreateUpdate`, mesa);
}

public createMesa(mesa): Observable<any> {
  return this.http.put(`${this.url}/api/Mesas/CreateUpdate`, mesa);
}

public deleteMesa(idMesa): Observable<any> {
  if ( this.params.has('idMesa')) {
    this.params = this.params.delete('idMesa');
    this.params = this.params.append('idMesa', idMesa);
    return this.http.delete(`${this.url}/api/Mesas`, { params: this.params});
  } else {
    this.params = this.params.append('idMesa', idMesa);
    return this.http.delete(`${this.url}/api/Mesas`, { params: this.params});
  }
}

public liberateMesa(idMesa):Observable<any> {
  return this.http.post(`${this.urlPayFoodNowWebApi}/api/Mesas/liberar/`,  idMesa );
}

}
