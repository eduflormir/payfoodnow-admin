import { Injectable } from '@angular/core';

import { UsuarioService } from '../general/usuario.service';
import { Router , CanActivate} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private usuarioService: UsuarioService, private router: Router) { }
  canActivate(): boolean {
  //   if (!this.usuarioService.isLoggin) {
  //     this.router.navigate(['login']);
  //     return false;
  //   }
  //   return true;
  // }

  return true;
  }
}
