export class CamareroMesa {
  IdCamareroMesa?: number;
  IdCamarero: number;
  IdMesa: number;


  constructor( IdCamareroMesa: number, IdCamarero: number, IdMesa: number ) {
    this.IdCamareroMesa = IdCamareroMesa;
    this.IdCamarero = IdCamarero;
    this.IdMesa = IdMesa;
  }
}
