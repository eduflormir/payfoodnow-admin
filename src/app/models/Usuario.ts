
export class Usuario {
  idUsuario: number;
  idCentro: number;
  nombre: string;
  apellidos: string;

  constructor(idUsuario,idCentro, nombre, apellidos ) {
    this.idUsuario = idUsuario;
    this.idCentro = idCentro;
    this.nombre = nombre;
    this.apellidos = apellidos;

  }
}



