import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './guards/AuthGuardService';

const routes: Routes = [
  
  {
    path: '' , loadChildren: './general/general.module#GeneralModule'
  },
  {
    path: '**' , redirectTo: 'login', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
