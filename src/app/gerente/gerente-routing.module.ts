import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalleGerenteComponent } from './detalle-gerente/detalle-gerente.component';


const routes: Routes = [

  {
    path: '', component: DetalleGerenteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GerenteRoutingModule { }
