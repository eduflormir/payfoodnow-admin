import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleGerenteComponent } from './detalle-gerente.component';

describe('DetalleGerenteComponent', () => {
  let component: DetalleGerenteComponent;
  let fixture: ComponentFixture<DetalleGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
