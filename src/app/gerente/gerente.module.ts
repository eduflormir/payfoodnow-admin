import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { GerenteRoutingModule } from './gerente-routing.module';
import { DetalleGerenteComponent } from './detalle-gerente/detalle-gerente.component';

@NgModule({
  declarations: [DetalleGerenteComponent],
  imports: [
    CommonModule,
    GerenteRoutingModule
  ]
})
export class GerenteModule { }
