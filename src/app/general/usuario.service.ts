import { Injectable } from '@angular/core';
import { Subject, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Usuario } from '../models/Usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  public headers = new HttpHeaders();

  [x: string]: any;

  url = environment.url;

  isLogginSubject = new Subject<any>();
  isLooginObservable$ = this.isLogginSubject.asObservable();
  isLoggin: boolean;

  usuarioSession: Usuario;


  constructor(private http: HttpClient) {
   }

   loginGerente(Username: string , Password: string) {

    const user: any = {};

    user.Username = Username;
    user.Password = Password;

    const  jsonBody = JSON.stringify(user);

    // const headers = this.headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(`${this.url}/api/Login/Validate`, jsonBody);
   }

   setLocalStorage(data) {
    localStorage.setItem('usuario', JSON.stringify(data));
    this.getLocalStorageUserJSON();
   }

  getLocalStorageUserJSON() {
    let usuarioData = localStorage.getItem('usuario');
    usuarioData = JSON.parse(usuarioData);
    return usuarioData;
   }

  isLogin(valor: boolean) {
    console.log('valor:', valor)
    console.log('URL', this.url)
    this.isLoggin = valor;
    return  this.isLogginSubject.next(valor);
  }
}
