import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent} from './login/login.component';
import { ListaMesasComponent } from '../mesas/lista-mesas/lista-mesas.component';
import { PrivadoComponent } from '../privado/privado/privado.component';
import { AuthGuardService } from '../guards/AuthGuardService';
import { EstablecimientoComponent } from './establecimiento/establecimiento.component';


const routesGeneral: Routes = [

{
  path: 'login', component: LoginComponent,
},
{
  path: 'establecimiento', component: EstablecimientoComponent,
},
{
  path: 'privado', loadChildren: '../privado/privado.module#PrivadoModule', canActivate: [AuthGuardService]
},
{
  path: '**' , redirectTo: 'login', pathMatch: 'full'
}


];

@NgModule({
  imports: [RouterModule.forChild(routesGeneral)],
  exports: [RouterModule]
})
export class GeneralRoutingModule { }
