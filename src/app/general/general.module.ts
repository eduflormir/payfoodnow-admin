import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// Importamos el módulo pricado, mesas y nuestro routing module
import { PrivadoModule } from '../privado/privado.module';
import { MesasModule } from '../mesas/mesas.module';
import { GeneralRoutingModule } from './general-routing.module';

import { LoginComponent } from './login/login.component';
import { EstablecimientoComponent } from './establecimiento/establecimiento.component';


@NgModule({
  declarations: [
    LoginComponent,
    EstablecimientoComponent
  ],
  imports: [
    CommonModule,
    GeneralRoutingModule,
    PrivadoModule,
    MesasModule,
    ReactiveFormsModule,
    FormsModule


  ],
  exports: [

  ]
})
export class GeneralModule { }
