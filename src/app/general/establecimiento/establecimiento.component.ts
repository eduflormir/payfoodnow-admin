import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from '../usuario.service';
import { LocalService } from 'src/app/services/local.service';



@Component({
  selector: 'app-establecimiento',
  templateUrl: './establecimiento.component.html',
  styleUrls: ['./establecimiento.component.scss']
})
export class EstablecimientoComponent implements OnInit {

  isLoggin: boolean = false;
  formularioLogin: FormGroup;
  usuario: any;
  idLocal: number;
  idCliente: number;
  public listadoUbicaciones = [];
  public formularioUbicacion: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private localService: LocalService
  ) {

   }

  ngOnInit() {
    this.getUsuario();
    // this.getIdCliente();
    this.formularioUbicacion = this.fb.group({
      ubicacion: ['']
    });
  }



  // Getter method to access formcontrols
  get ubicacion() {
    return this.formularioUbicacion.get('ubicacion');
  }

  async getUsuario() {
    this.usuario = await this.usuarioService.getLocalStorageUserJSON();
    console.log('USUARIO', this.usuario)
    this.getAll(this.usuario.idUsuario)
    // this.getIdCliente(this.usuario.idLocal);
    // this.idLocal = this.usuario.idLocal;

  }

  getIdCliente(idLocal) {
    console.log('idLocal', idLocal)
    this.localService.getLocal(idLocal)
    .subscribe( (res: any) => {
      this.idCliente = res.Result.idCliente;
      console.log('IdCliente', this.idCliente);
      this.getAll(this.idCliente);
    });
  }

  login() {
    // this.localService.getLocal(this.idLocal)
    //   .subscribe( (res: any) => {
    //     console.log('RES', res)
    //     this.idCliente = res.Result.idCliente;
    //     console.log('IDCliente', this.idCliente)

    //   });
}

accederMenuPrincipal(event) {
  console.log(event);
}

getAll(IdCliente) {
  this.localService.getAll(IdCliente)
    .subscribe( (res: any) => {
      this.listadoUbicaciones = res.Result;
      console.log('Respuesta desde getAll', res, this.listadoUbicaciones);

    });
}

submit($event) {
    localStorage.setItem('idLocal', $event.ubicacion);
    this.router.navigate(['privado', 'mesas']);
}

}



