import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../../models/Usuario';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  isLoggin: boolean = false;
  formularioLogin: FormGroup;
  constructor(
    private router: Router,
    private fb : FormBuilder,
    private _usuarioService: UsuarioService
  ) { }
  ngOnInit() {
    this.formularioLogin = this.fb.group({
      usuario: ['', Validators.required],
      password: ''
    })
  }
  login(respuesta) {
    const usuario = respuesta.value.usuario;
    const password = respuesta.value.password;

    if ( usuario != null && password != null) {
      this._usuarioService.loginGerente(usuario, password)
      .subscribe( (data) => {
        this._usuarioService.setLocalStorage(data);

        this.router.navigate(['establecimiento']);

      });

    //this.router.navigate(['privado', 'mesas', 'detalleMesas']);
    //    this.isLoggin = true;
    //    this._usuarioService.isLogin(this.isLoggin);
    //    this.router.navigate(['establecimiento']);
    //  } else {
    //    this.isLoggin == false;
     }
  }
}
